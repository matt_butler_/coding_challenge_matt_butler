class Task 
  include ActiveModel::Validations
  include ActiveModel::Serializers::JSON

  MILLISECONDS_TO_SECONDS = 1000
  HOURS_TO_DAYS = 24

  attr_accessor :id, :start_date, :request_date, :end_date, :status, 
                :total, :remaining, :processed, :username, :fullname, :email,
                :image_choice, :standard_status, :interpreted_status

  def attributes=(hash)
    hash.each do |key, value|
      if key == "request_date" && value
        send("#{key}=", format_date_time(value))
      elsif key == "end_date" && value
        send("#{key}=", format_date_time(value))
      elsif key == "remaining" && value
        send("#{key}=", format_remaining_time(value))
      else
        send("#{key}=", value)
      end
    end
  end

  def attributes
    instance_values
  end

  def assign_additional_fields
    inactive = !task.start_date
    error = task.start_date && task.end_date && (task.total != task.processed)
    success = task.start_date && task.end_date && (task.total == task.processed) 
    in_progress = task.start_date && !task.end_date && (task.total != task.processed)

    if(inactive) 
      task.standard_status = 'INACTIVE'
      task.image_choice = 'icon-inactive.png'
      task.interpreted_status = 'NOT STARTED'
    elsif (error)
      task.standard_status = 'ERROR'
      task.image_choice = 'icon-error.png'
      task.interpreted_status = 'Halted '.concat(task.end_date)
    elsif (success)
      task.standard_status = 'SUCCESS'
      task.image_choice = 'icon-success.png'
      task.interpreted_status = 'Completed '.concat(task.end_date)
    elsif (in_progress)
      task.standard_status = 'IN PROGRESS'
      task.image_choice = 'icon-inprogress.png'
      task.interpreted_status = 'Time Remaining '.concat(task.remaining.to_s)
    end


    helper = Object.new.extend(ActionView::Helpers::NumberHelper)
    if !task.processed
      task.processed = 0
    end 
    task.processed = helper.number_to_human_size(task.processed, :precision => 2, :separator => '.')
    
    if task.total
      task.total = helper.number_to_human_size(task.total, :precision => 2, :separator => '.')
    end
  end

  def format_date_time(dt)
    DateTime.parse(dt).strftime("%m/%d/%Y %H:%M %p %z")
  end

  def format_remaining_time(rt)
    
    rt = rt/MILLISECONDS_TO_SECONDS
    mm, ss = rt.divmod(60)
    hh, mm = mm.divmod(60)
    dd = hh/HOURS_TO_DAYS

    if dd > 2
      "%d days" % [dd]
    else
      "%d:%d:%d" % [hh, mm, ss]
    end
  end
end
