class TasksController < ApplicationController
  


  def index
  end

  def create
    @all_tasks = []
    
    file_name = params[:json_file]
    
    if file_name == nil
      flash[:error] = 'Please select a valid json file.'
      redirect_to root_path
      return
    end


    begin
      file_content = JSON.parse file_name.read
    rescue Exception => exc
      flash[:error] = "The file you have selected cannot be parsed! Please check that the file is valid JSON."
      redirect_to root_path
      return
    end

    all_json_tasks = file_content['DATA']

    all_json_tasks.each do |t|

      task = Task.new
      task.from_json(t.to_json)


      inactive = !task.start_date
      error = task.start_date && task.end_date && (task.total != task.processed)
      success = task.start_date && task.end_date && (task.total == task.processed) 
      in_progress = task.start_date && !task.end_date && (task.total != task.processed)

      if(inactive) 
        task.standard_status = 'INACTIVE'
        task.image_choice = 'icon-inactive.png'
        task.interpreted_status = 'NOT STARTED'
      elsif (error)
        task.standard_status = 'ERROR'
        task.image_choice = 'icon-error.png'
        task.interpreted_status = 'Halted '.concat(task.end_date)
      elsif (success)
        task.standard_status = 'SUCCESS'
        task.image_choice = 'icon-success.png'
        task.interpreted_status = 'Completed '.concat(task.end_date)
      elsif (in_progress)
        task.standard_status = 'IN PROGRESS'
        task.image_choice = 'icon-inprogress.png'
        task.interpreted_status = 'Time Remaining '.concat(task.remaining.to_s)
      end


      helper = Object.new.extend(ActionView::Helpers::NumberHelper)
      if !task.processed
        task.processed = 0
      end 
      task.processed = helper.number_to_human_size(task.processed, :precision => 2, :separator => '.')
      
      if task.total
        task.total = helper.number_to_human_size(task.total, :precision => 2, :separator => '.')
      end

      @all_tasks << task
    end
  end
end