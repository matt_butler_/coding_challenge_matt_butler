Rails.application.routes.draw do
  get 'tasks/create'
  post 'tasks/create'
  get 'tasks/index'
  post 'tasks/index'
  root to: 'tasks#index'
end
